package foreman

import (
	"bazil.org/fuse"
	"bazil.org/fuse/fs"
	"context"
	"git.lysator.liu.se/hugo/foremanFS/fetch"
	"os"
	"syscall"
)

// directory for a single puppetclass
type HostClasses struct{ hostname string }

func (HostClasses) Attr(ctx context.Context, a *fuse.Attr) error {
	a.Inode = 0
	a.Mode = os.ModeDir | 0o555
	return nil
}

func (d HostClasses) Lookup(ctx context.Context, name string) (fs.Node, error) {
	classes := fetch.GetClassList(fetch.Hosts[d.hostname].Data)

	for _, class := range classes {
		if name == class {
			return PuppetClass{class}, nil
		}
	}

	return nil, syscall.ENOENT
}

func (d HostClasses) ReadDirAll(ctx context.Context) ([]fuse.Dirent, error) {
	classes := fetch.GetClassList(fetch.Hosts[d.hostname].Data)

	var out []fuse.Dirent
	for _, class := range classes {
		out = append(out, fuse.Dirent{Inode: 0, Name: class, Type: fuse.DT_Link})
	}
	return out, nil
}
