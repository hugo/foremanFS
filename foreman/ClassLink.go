package foreman

import (
	"bazil.org/fuse"
	"context"
	"os"
)

type PuppetClass struct{ Classname string }

func (PuppetClass) Attr(ctx context.Context, a *fuse.Attr) error {
	a.Inode = 0
	a.Mode = os.ModeSymlink | 0o444
	return nil
}

func (c PuppetClass) Readlink(ctx context.Context, req *fuse.ReadlinkRequest) (string, error) {

	return "../../../classes/" + c.Classname, nil
	// return "[empty]", nil
}
