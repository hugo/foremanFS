package foreman

import (
	"bazil.org/fuse"
	"bazil.org/fuse/fs"
	"context"
	"git.lysator.liu.se/hugo/foremanFS/fetch"
	"os"
	"syscall"
)

// list of all known puppet classes
type ClassList struct{}

func (ClassList) Attr(ctx context.Context, a *fuse.Attr) error {
	a.Inode = 0
	a.Mode = os.ModeDir | 0o555
	return nil
}

func (ClassList) Lookup(ctx context.Context, name string) (fs.Node, error) {
	for key, _ := range fetch.AllClasses {
		if key == name {
			return PuppetClass{name}, nil
		}
	}
	return nil, syscall.ENOENT
}

func (ClassList) ReadDirAll(ctx context.Context) ([]fuse.Dirent, error) {
	var dirents []fuse.Dirent
	for key, _ := range fetch.AllClasses {
		dirents = append(dirents, fuse.Dirent{Inode: 0, Name: key, Type: fuse.DT_Dir})
	}
	return dirents, nil
}

