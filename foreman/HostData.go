package foreman

import (
	"bazil.org/fuse"
	"context"
	"encoding/json"
	"git.lysator.liu.se/hugo/foremanFS/fetch"
	"log"
	_ "fmt"
)

type HostData struct {
	Hostname string
}

func (f HostData) Attr(ctx context.Context, a *fuse.Attr) error {
	a.Inode = 0
	a.Mode = 0o444
	// a.Size = uint64(len(fmt.Sprintf("%+v\n", f.record)))
	// a.Size = uint64(len(greeting))
	// a.Size = uint64(len(fmt.Sprintf("%+v\n", fetch.Hosts[f.Hostname].HostData)))
	data := fetch.Hosts[f.Hostname].Data
	out, err := json.MarshalIndent(data, "", "    ")
	if err != nil {
		log.Fatal(err)
	}
	a.Size = uint64(len(out))
	return nil
}

func (f HostData) ReadAll(ctx context.Context) ([]byte, error) {
	data := fetch.Hosts[f.Hostname].Data
	out, err := json.MarshalIndent(data, "", "    ")
	if err != nil {
		log.Fatal(err)
	}
	// return []byte(fmt.Sprintf("%+v\n", data)), nil
	return out, nil
}

