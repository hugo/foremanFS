package foreman

import (
	"bazil.org/fuse"
	"context"
	"strings"
)

type RequestScript struct {
	Initialized bool
	Data string
}

const apiurl = "https://chapman.lysator.liu.se/api/"

func (r *RequestScript) Init() error {
	var sb strings.Builder
	sb.WriteString("#!/bin/bash\n")
	sb.WriteString("curl --silent --insecure ")
	sb.WriteString("--user \"hugo:$(pass lysator/hugo)\" ");
	sb.WriteString(apiurl);
	sb.WriteString("/$1 \\\n")
	sb.WriteString("	| jq '.'\n")
	r.Data = sb.String()
	r.Initialized = true
	return nil
}

func (r RequestScript) Attr(ctx context.Context, a *fuse.Attr) error {
	a.Inode = 0
	a.Mode = 0o555
	if (! r.Initialized) {
		r.Init()
	}
	bytes := []byte(r.Data)
	l := len(bytes)
	a.Size = uint64(l)
	return nil
}

func (r RequestScript) ReadAll(ctx context.Context) ([]byte, error) {
	if (! r.Initialized) {
		r.Init()
	}
	return []byte(r.Data), nil
}

