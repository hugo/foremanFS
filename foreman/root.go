package foreman

import (
	"bazil.org/fuse"
	"bazil.org/fuse/fs"
	"context"
	"os"
	"syscall"
)

// root of file system
type Root struct{}

func (Root) Attr(ctx context.Context, a *fuse.Attr) error {
	a.Inode = 0
	a.Mode = os.ModeDir | 0o555
	return nil
}

func (Root) Lookup(ctx context.Context, name string) (fs.Node, error) {

	switch name {
	case "hosts":
		return HostList{}, nil
	case "classes":
		return ClassList{}, nil
	case "request":
		return RequestScript{}, nil
	default:
		return nil, syscall.ENOENT
	}
}

func (Root) ReadDirAll(ctx context.Context) ([]fuse.Dirent, error) {
	return []fuse.Dirent{
		{Inode: 0, Name: "hosts", Type: fuse.DT_Dir},
		{Inode: 0, Name: "classes", Type: fuse.DT_Dir},
		{Inode: 0, Name: "request", Type: fuse.DT_File},
	}, nil
}
