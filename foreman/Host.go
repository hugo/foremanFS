package foreman

import (
	"bazil.org/fuse"
	"bazil.org/fuse/fs"
	"context"
	"os"
	"syscall"
)


// directory for a single host (machine)
type Host struct{ hostname string }

func (Host) Attr(ctx context.Context, a *fuse.Attr) error {
	a.Inode = 0
	a.Mode = os.ModeDir | 0o555
	return nil
}


func (d Host) Lookup(ctx context.Context, name string) (fs.Node, error) {
	switch name {
	case "data":
		return HostData{d.hostname}, nil
	case "classes":
		return HostClasses{d.hostname}, nil
	}
	return nil, syscall.ENOENT
}

func (Host) ReadDirAll(ctx context.Context) ([]fuse.Dirent, error) {
	return []fuse.Dirent{
		{Inode: 0, Name: "data", Type: fuse.DT_File},
		{Inode: 0, Name: "classes", Type: fuse.DT_Dir},
	}, nil
}

