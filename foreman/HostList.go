package foreman

import (
	"bazil.org/fuse"
	"bazil.org/fuse/fs"
	"context"
	"git.lysator.liu.se/hugo/foremanFS/fetch"
	"os"
	"syscall"
)


// list of all known hosts (machines)
type HostList struct{}

func (HostList) Attr(ctx context.Context, a *fuse.Attr) error {
	a.Inode = 0
	a.Mode = os.ModeDir | 0o555
	return nil
}

func (HostList) Lookup(ctx context.Context, name string) (fs.Node, error) {
	_, ok := fetch.Hosts[name]
	if !ok {
		return nil, syscall.ENOENT
	}
	return Host{name}, nil
}

func (HostList) ReadDirAll(ctx context.Context) ([]fuse.Dirent, error) {
	var dirents []fuse.Dirent
	for _, item := range fetch.Hosts {
		dirents = append(dirents, fuse.Dirent{Inode: 0, Name: item.Data.Name, Type: fuse.DT_Dir})
	}
	return dirents, nil
}
