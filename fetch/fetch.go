package fetch

import (
	"fmt"
	"log"
	"math"

	"crypto/tls"
	"encoding/json"
	"net/http"
)

var Hosts = map[string]Host{}

const apiurl = "https://chapman.lysator.liu.se/api/"

// which classes a given host has
var hostClasses map[float64][]Puppetclass

var AllClasses map[string]Puppetclass

// password for foreman access, TODO replace with API key
var password string

var client *http.Client

// echo curl --user "hugo:$(pass lysator/hugo)" -k https://chapman.lysator.liu.se/api/hosts

type Host struct {
	Data HostRecord
}

func Init(pw string) {

	password = pw

	hostClasses = map[float64][]Puppetclass{}
	AllClasses = map[string]Puppetclass{}

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}

	client = &http.Client{Transport: tr}

	var m HostMessage
	fff_pages("hosts", &m)

	for _, record := range m.Results {
		Hosts[record.Name] = Host{record}
	}

	var m2 PuppetclassMessage

	fff_pages("puppetclasses", &m2)

	for group, entries := range m2.Results {
		log.Print("Handling ", group)
		for _, entry := range entries {
			// e := entry.(map[string]interface{})
			AllClasses[entry.Name] = entry
		}
	}
}

func fff (url string, message interface{}) {

	request, err := http.NewRequest("GET", apiurl + url, nil)

	if err != nil {
		log.Fatal(err)
	}
	request.SetBasicAuth("hugo", password)

	resp, err := client.Do(request)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()


	decoder := json.NewDecoder(resp.Body)

	err = decoder.Decode(message)
	if err != nil {
		log.Fatal(err)
	}

}

func fff_pages(url string, message IsMessage) {
	fff(url, &message)
	log.Printf("message = %+v", message.GetMessage())

	m := message.GetMessage()
	pagecount := int(math.Ceil(m.Total / m.Per_page))
	for i := 2; i < pagecount; i++ {
		// TODO this kind of works for dictionaries, since json.Unmarshal
		// reuses an existing dictionary.  I believe that it however
		// "fails" if the same key is present in multiple responses, and
		// that it doesn't work for list responses.
		// TODO also, all these should run in paralell
		fff(url + fmt.Sprintf("?page=%d", i),
			&message)
		log.Printf("message = %+v", message.GetMessage())
	}
}

func GetClassList(host HostRecord) []string {
	var record []Puppetclass

	if val, ok := hostClasses[host.Id]; ok {
		record = val
	} else {
		url := fmt.Sprintf("hosts/%d/puppetclasses", int(host.Id))
		var m PuppetclassMessage
		fff_pages(url, &m)

		for _, values := range m.Results {
			record = append(record, values...)
		}
		hostClasses[host.Id] = record
	}

	str := []string{}
	for _, r := range record {
		str = append(str, r.Name)
	}
	return str
}

type HostRecord struct {
	Ip               string  // "130.236.254.139",
	Ip6              string  // "2001:6b0:17:f0a0::8b",
	Environment_id   float64 // 1,
	Environment_name string  // "production",
	Last_report      string  // "2020-10-28 19:10:57 UTC",
	Mac              string  // "1c:c1:de:03:e7:b6",
	/*
		"realm_id": null,
		"realm_name": null,
		"sp_mac": null,
		"sp_ip": null,
		"sp_name": null,
	*/
	Domain_id            float64 // 1,
	Domain_name          string  // "lysator.liu.se",
	Architecture_id      float64 // 1,
	Architecture_name    string  // "x86_64",
	Operatingsystem_id   float64 // 6,
	Operatingsystem_name string  // "CentOS Linux 7.8.2003",
	/*
		"subnet_id": null,
		"subnet_name": null,
		"subnet6_id": null,
		"subnet6_name": null,
		"sp_subnet_id": null,
		"ptable_id": null,
		"ptable_name": null,
		"medium_id": null,
		"medium_name": null,
		"pxe_loader": null,
		"build": false,
	*/
	Comment string // "",
	/*
		"disk": null,
		"installed_at": null,
	*/
	Model_id     float64 // 3,
	Hostgroup_id float64 // 6,
	Owner_id     float64 // 5,
	Owner_name   string  // "Henrik Henriksson",
	Owner_type   string  // "User",
	Enabled      bool    // true,
	Managed      bool    // false,
	/*
		"use_image": null,
	*/
	Image_file string // "",
	/*
		"uuid": null,
		"compute_resource_id": null,
		"compute_resource_name": null,
		"compute_profile_id": null,
		"compute_profile_name": null,
	*/
	// capabilities []interface{} // [ "build" ],
	Provision_method string // "build",
	Certname         string // "analysator-system.lysator.liu.se",
	/*
		"image_id": null,
		"image_name": null,
	*/
	Created_at string // "2020-08-16 19:51:59 UTC",
	Updated_at string // "2020-10-28 19:11:26 UTC",
	/*
		"last_compile": null,
	*/
	Global_status              float64 // 0,
	Global_status_label        string  // "OK",
	Uptime_seconds             float64 // 20857465,
	Organization_id            float64 // 1,
	Organization_name          string  // "Lysator",
	Location_id                float64 // 2,
	Location_name              string  // "Foo",
	Puppet_status              float64 // 0,
	Model_name                 string  // "ProLiant DL180 G6",
	Configuration_status       float64 // 0,
	Configuration_status_label string  // "No changes",
	Name                       string  // "analysator-system.lysator.liu.se",
	Id                         float64 // 13,
	Puppet_proxy_id            float64 // 1,
	Puppet_proxy_name          string  // "chapman.lysator.liu.se",
	Puppet_ca_proxy_id         float64 // 1,
	Puppet_ca_proxy_name       string  // "chapman.lysator.liu.se",
	/*
		"puppet_proxy": {
			"name": "chapman.lysator.liu.se",
			"id": 1,
			"url": "https://chapman.lysator.liu.se:8443"
		},
		"puppet_ca_proxy": {
			"name": "chapman.lysator.liu.se",
			"id": 1,
			"url": "https://chapman.lysator.liu.se:8443"
		},
	*/
	Hostgroup_name  string // "System",
	Hostgroup_title string // "Analysator/System"
}


type IsMessage interface {
	GetMessage() Message
	// TODO
	// MergeMessage(Message) Message
}

type Message struct {
	Total, Subtotal, Page, Per_page float64
	// search
	// sort { by, order null }
}

type HostMessage struct {
	Message
	Results []HostRecord
}

func (m HostMessage) GetMessage() Message {
	return m.Message
}

type Puppetclass struct {
	Id         float64
	Name       string // "profiles::localtime"
	Created_at string // "2020-10-27T07:57:47.102Z"
	Updated_at string // "2020-10-27T07:57:47.102Z"
}

type PuppetclassMessage struct {
	Message
	Results map[string][]Puppetclass
}

func (m PuppetclassMessage) GetMessage() Message {
	return m.Message
}
