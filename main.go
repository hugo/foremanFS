package main

import (
	"log"
	"os"

	"git.lysator.liu.se/hugo/foremanFS/foreman"
	"git.lysator.liu.se/hugo/foremanFS/fetch"

	"bazil.org/fuse"
	"bazil.org/fuse/fs"
)

func main() {
	password := os.Args[1:][0]

	// TODO fail on no password

	fetch.Init(password)

	c, err := fuse.Mount(
		"mnt",
		fuse.FSName("Foreman"),
		fuse.Subtype("foreman"),
	)
	if err != nil {
		log.Fatal(err)
	}
	// defer c.Unmount()
	defer c.Close()

	log.Print("Serving filesystem")
	err = fs.Serve(c, FS{})
	if err != nil {
		log.Fatal(err)
	}

}

type FS struct{}

func (fs FS) Root() (fs.Node, error) {
	// return Dir{   /*fs.data*/ }, nil
	return foreman.Root{ /*fs.data*/ }, nil
}
